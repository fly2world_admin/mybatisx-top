# Navicat 操作手册 
Navicat是一套快速、可靠并价格相当便宜的数据库管理工具，专为简化数据库的管理及降低系统管理成本而设。
## 安装手册
>Navicat安装 

百度网盘链接：https://pan.baidu.com/s/1gst4g7ePsUf2VdiHIYZhWw
提取码：wasd

![](https://cdn.jsdelivr.net/gh/fly2world/picgo/chao/Navicat%E5%AE%89%E8%A3%85.png)
解压完成后进行安装，一直下一步直到安装成功。
安装成功后打开Navicat，显示使用期限只有15天，这时候就需要进行安装包的另一项设置。
![](https://cdn.jsdelivr.net/gh/fly2world/picgo/chao/Navicat%E7%A0%B4%E8%A7%A3.png)
破解Navicat之前需要先关闭网络以及电脑自身的防火墙，不然会提示为病毒文件，直接删除安装文件。
按照此文件夹中的步骤进行破解，Navicat就可以正常使用了。
## 必要配置

>Navicat连接本地Mysql数据库 

1.必要配置，在电脑中已经安装好Mysql，并且环境变量已配置好。
![](https://cdn.jsdelivr.net/gh/fly2world/picgo/chao/%E6%9C%AC%E5%9C%B0%E6%95%B0%E6%8D%AE%E5%BA%93.png)
2.需要注意到，如果连接失败，很有可能是Mysql的安装错误导致。

因项目大小以及电脑配置无法确定，所以推荐使用远程数据库来进行操作。

>Navicat连接远程Mysql数据库
 
1.获取远程数据库的信息（注：确保账号密码无误）
![](https://cdn.jsdelivr.net/gh/fly2world/picgo/chao/%E8%BF%9E%E6%8E%A5%E8%BF%9C%E7%A8%8B%E6%95%B0%E6%8D%AE%E5%BA%93.png)

2.建立连接后，就可以开始新建数据库
![](https://cdn.jsdelivr.net/gh/fly2world/picgo/chao/%E6%96%B0%E5%BB%BA%E6%95%B0%E6%8D%AE%E5%BA%93.png) 

3.数据库建好后，就可以开始新建表
![](https://cdn.jsdelivr.net/gh/fly2world/picgo/chao/%E6%96%B0%E5%BB%BA%E8%A1%A8.png)

4.新建表后，可以新建查询的以及设计表的方式来对表数据进行增删改查。
![](https://cdn.jsdelivr.net/gh/fly2world/picgo/chao/%E6%96%B0%E5%BB%BA%E6%9F%A5%E8%AF%A2.png)

>Navicat中SSH的使用介绍

在Navicat中，SSH功能允许你建立一个安全的连接来管理远程数据库服务器。
通过使用SSH，你可以在本地计算机上直接访问位于远程服务器上的数据库。
第一步：
![](https://cdn.jsdelivr.net/gh/fly2world/picgo/chao/SSH%E8%AE%BE%E7%BD%AE1.png)

第二步：
![](https://cdn.jsdelivr.net/gh/fly2world/picgo/chao/SSH%E9%80%9A%E9%81%932.png)


