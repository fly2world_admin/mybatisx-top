# Vitepress操作手册

Vitepress是使用Vue3+Vite来快速搭建一个个人网站的工具，网站搭建者不需要掌握Vue3，Vite等的具体内容，只需要简单的配置就可以生成Vue风格的个人网站。

## 官方地址
详情了解 https://vitepress.dev/guide/markdown

官方地址提供了在线试用，帮助搭建一个基础的Vitepress框架，此外还需要有一定的js基础才可以使页面变得富有层次

## 安装条件
Vitepress使用前必须安装node.js,强烈推荐使用nvm，切换node更为便捷。

详情可参考本网站下nvm的安装:
http://127.0.0.1:5173/fore-view/basic/start.html  
## 配置文件
配置文件在`.vitepress/config.js`目录下，有关于网站的门面介绍以及排版内容都在这里展示。  
下方是本网站的基础信息配置，仅供参考使用。
```js
module.exports = {
    lang: 'zh-CN',
    title: '技术分享',
    description: '欢迎大家交流,微信 fishbone997 qw45278',
    head: [
        ["link", {rel: "icon", href: "/logo.svg"}], // favicon
    ],
    vite: {
        plugins: [
            // add plugin
            AutoSidebar()
        ]
    }
```
## 优化网站
网站的优化体现在网站的主题格式，基础排版等页面的展示效果，详情可参考下方链接进行网站的优化。  
优化: https://vitepress.dev/reference/site-config  
以上就是Vitepress的基础开发教程，后续工具组件的教程可持续关注本网站。
