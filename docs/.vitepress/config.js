import AutoSidebar from 'vite-plugin-vitepress-auto-sidebar';

module.exports = {
    lang: "zh-CN",
    title: "技术分享",
    description: "欢迎大家交流,微信 fishbone997 qw45278",
    head: [
        ["link", { rel: "icon", href: "/logo.svg" }], // favicon
    ],
    vite: {
        plugins: [
            // add plugin
            AutoSidebar(),
        ],
    },
    themeConfig: {
        siteTitle: "鱼刺", //左上角的
        logo: "/logo.svg",
        search: {
            provider: "local",
        },
        // 以下参考 https://vitepress.dev/reference/default-theme-sidebar
        nav: [
            { text: "Get Started", link: "/guide" },
            /*{
                text: "产品",
                items: [
                    {text: "设计", link: "/articles/0基础/index"},
                    {text: "体验", link: "/articles/vue/index"},
                    {text: "Shell", link: "/articles/react/index"},
                ],
            },*/
            {
                text: "前端",
                items: [
                    { text: "基础", link: "/fore-view/0基础/0.指南" },
                    { text: "Electron", link: "/fore-view/Electron/0.起步" },
                    { text: "Three3D", link: "/fore-view/Three3D/地图" },
                    { text: "Vue", link: "/fore-view/Vue/Vite创建项目" },
                    { text: "小程序", link: "/fore-view/小程序/H5打开小程序" },
                    // {text: "Web3D", link: "/articles/web3d/index"},
                    // {text: "其他", link: "/articles/other/vitepress/index"},
                ],
            },
            {
                text: "后端",
                items: [
                    { text: "Go", link: "/server/Go/0.指南" },
                    { text: "Java", link: "/server/Java/0.指南" },
                    // {text: "Rest", link: "/server/rust/index"},
                    // {text: "其他", link: "/server/other/index"},
                ],
            },
            /*{
                text: "数据库",
                items: [
                    {text: "Mysql", link: "/articles/0基础/index"},
                    {text: "PostgreSql", link: "/articles/vue/index"},
                    {text: "Redis", link: "/articles/react/index"},
                    {text: "MQ", link: "/articles/react/index"},
                    {text: "其他", link: "/articles/other/vitepress/index"},
                ],
            },*/
            /*{
                text: "测试",
                items: [
                    {text: "K8s", link: "/articles/0基础/index"},
                    {text: "Docker", link: "/articles/vue/index"},
                    {text: "Shell", link: "/articles/react/index"},
                ],
            },*/
            {
                text: "运维",
                items: [
                    { text: "视频监控", link: "/operate/ffmpeg/视频监控" },
                    { text: "MQ", link: "/operate/mq/RabbitMQ" },
                    { text: "Ipv6公网", link: "/operate/network/ipv6" },
                    // {text: "K8s", link: "/articles/0基础/index"},
                    // {text: "Docker", link: "/articles/vue/index"},
                    // {text: "Shell", link: "/articles/react/index"},
                ],
            },
            {
                text: "工具",
                items: [
                    // {text: "Idea", link: "/articles/0基础/index"},
                    // {text: "Vscode", link: "/articles/vue/index"},
                    { text: "Navicat", link: "/tools/Navicat" },
                    { text: "Vitepress", link: "/tools/VitePress" },
                    { text: "小工具", link: "/tools/小工具" },
                ],
            },
        ],
        socialLinks: [{ icon: "github", link: "https://gitee.com/fly2world_admin" }],
        footer: {
            // message: 'Apache License 2.0.',
            copyright: 'Copyright © 2019-present <a href="http://mybatisx.top">mybatisx.top</a> | <a href="https://beian.miit.gov.cn/">晋ICP备2023021957号-1</a>',
        },
    },
};
