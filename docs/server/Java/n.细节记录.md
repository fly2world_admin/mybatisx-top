---
sidebar: auto
---
# 细节记录

记录一些常用写法

## 常用用法

> git记住密码
```shell
git config --global credential.helper store
```
> MAVEN导入本地文件
```xml

<dependencies>
    <dependency>
        <groupId>com.example</groupId>
        <artifactId>example</artifactId>
        <version>1.0.0</version>
        <scope>system</scope>
        <systemPath>${project.basedir}/lib/example.jar</systemPath>
    </dependency>
</dependencies>
```



