---
sidebar:auto
---

# 环境搭建
## nvm搞定万能node版本
> nvm安装

官网地址 https://nvm.uihtm.com/  
下载后一直下一步，直到安装成功。
> 查看可安装的版本

nvm list available

> 安装node环境

比如 16.18.0
```shell
nvm install 16.18.0
nvm use 16.18.0
```
## 切换版本
```shell
nvm use 16.18.0
```
