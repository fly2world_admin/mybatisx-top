---
sidebar:auto
---

# Vite 创建项目

## 官网

起步地址: https://cn.vitejs.dev/guide/

## 创建项目

```shell
npm create vite@latest
```

输入项目名,选择 vue, JavaScript

创建成功!

## 注意
```shell
.gitignore 文件中加入: .vscode
```

## 安装依赖
```shell
pnpm i
```
修改 package.json，加入 --host 开通局域网访问
```shell
"scripts": {
    "dev": "vite --host",
    ......
},
```

## 启动
```shell
pnpm run dev
```
