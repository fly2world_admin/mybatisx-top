---
sidebar:auto
---

# H5打开小程序
## 作用
- PC网页跳转
- H5打开
- 微信网页打开小程序
- 短信发送

## 效果

扫码直接体验   
![](https://cdn.jsdelivr.net/gh/fly2world/picgo/images/h5%E6%89%93%E5%BC%80%E5%B0%8F%E7%A8%8B%E5%BA%8F.png)

## 开发思路

- [官方文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/guide/staticstorage/jump-miniprogram.html)
- 基于云开发(开通腾讯云开发)
- [同一个公司主体可共用云](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/guide/resource-sharing/)

## 开发步骤

1. 下载官网[示例代码](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/guide/staticstorage/jump-miniprogram.html),点击下载那
2. 小程序新建云开发，将官网示例代码拷贝进去
3. 将下载的h5代码，修改后上传到自己服务器
4. 调整参数即可

## 调用过程
1. h5页面后面可跟参数 https://xxxxx?a=123
2. h5代码获取参数,并传入
```js
async function openWeapp(onBeforeJump) {
    const query = window.location.search.replace(/^\?/, "");  //获取query参数
    var c = window.c
    const res = await c.callFunction({
        name: 'public2',
        data: {
            action: 'getUrlScheme2',
            query: query,  //不能进行encodeURIComponent编码，不能传对象
        },
    })
    // alert(JSON.stringify(res))
    if (onBeforeJump) {
        onBeforeJump()
    }
    location.href = res.result.openlink
}
```
3. 云函数获取参数,并生成链接 index.js
```js
// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
    env: cloud.DYNAMIC_CURRENT_ENV
}) // 使用当前云环境

// 云函数入口函数
exports.main = async (event, context) => {
    const wxContext = cloud.getWXContext()

    switch (event.action) {
        case 'getUrlScheme2': {
            return getUrlScheme2(event.query)
        }
    }

    return 'action not found'
}

async function getUrlScheme2(query) {
    console.log('getUrlScheme2·query', query)
    return cloud.openapi({appid: 'wxAxxxxxxxxxx'}).urlscheme.generate({
        // return cloud.openapi.urlscheme.generate({
        jumpWxa: {
            path: 'pages/outer/outer', // <!-- replace -->
            query: query,
        },
        // 如果想不过期则置为 false，并可以存到数据库
        isExpire: false,
        // 一分钟有效期
        expireTime: parseInt(Date.now() / 1000 + 60),
    })
}
```
4. 小程序获取，通过onLoad函数的options获取
![小程序获取](https://cdn.jsdelivr.net/gh/fly2world/picgo/images/%E5%B0%8F%E7%A8%8B%E5%BA%8F%E8%8E%B7%E5%8F%96%E5%8F%82%E6%95%B0.png)  

搞定!
