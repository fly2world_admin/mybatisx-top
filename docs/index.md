---
title: 鱼刺
layout: home
hero:
  name: fishbone
  text: 一个不懂语言的开发者
  tagline: 欢迎技术交流,微信号 fishbone997 qw45278
  image:
    src: /logo.svg
    alt: 鱼刺个人博客
  actions:
    - theme: brand
      text: Get Started
      link: /guide
    - theme: alt
      text: View on Gitee
      link: https://gitee.com/fly2world_admin
features:
  - icon: ⚡️
    title: 来玩
    details: 技术不但能学，还要耐玩
  - icon: 🖖
    title: 踏坑
    details: 技术没问题，问题是坑多，避坑就来看本站
  - icon: 🛠️
    title: 交友
    details: 可合作写一些开源软件
---

