# RabbitMQ

## 参考

参考网址: https://blog.csdn.net/qq_44146008/article/details/126547064

## 查询rabbit 和 erlang 匹配网址

https://www.rabbitmq.com/which-erlang.html 找到对应版本号.

## 下载 erlang

下载网址: https://packagecloud.io/rabbitmq/erlang

**注意**：你是centos8 就下载 el8 , centos9 就下载 el9

```shell
curl -s https://packagecloud.io/install/repositories/rabbitmq/erlang/script.rpm.sh | sudo bash
sudo yum install erlang-26.1.2-1.el8.x86_64
```

## 下载 rabbit

下载网址: https://packagecloud.io/rabbitmq/rabbitmq-server

**注意**：你是centos8 就下载 el8 , centos9 就下载 el9

没错,就是强调两次,因为我踏坑了!

```shell
curl -s https://packagecloud.io/install/repositories/rabbitmq/rabbitmq-server/script.rpm.sh | sudo bash
sudo yum install rabbitmq-server-3.12.8-1.el8.noarch
```

至此,已安装成功~

## 配置服务

```shell
systemctl start rabbitmq-server
systemctl status rabbitmq-server
systemctl restart rabbitmq-server
systemctl enable rabbitmq-server
```

## 安装Web管理可视化界面插件

```shell
rabbitmq-plugins enable rabbitmq_management
systemctl restart rabbitmq-server
```

新增远程用户:

```shell
rabbitmqctl add_user admin admin123
```

分配权限:

```shell
rabbitmqctl set_user_tags admin administrator
```

## 访问

```shell
http://服务器ip:15672/
```

成功后, admin/admin123 登录即可!

